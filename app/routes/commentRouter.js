const express=require('express');
const router=express.Router();
const commentController=require('../modules/comments/commentController');

/*
  @author : Riduvan
  date : 19/10/2021
*/
router.route('/posts/:id/comments').get(commentController.getPostComments).post(commentController.savePostComments);
module.exports=router;