const express=require('express');
const router=express.Router();
const postController=require('../modules/posts/postController');
const commentController=require('../modules/posts/postController');

/*
  @author : Riduvan
  date : 19/10/2021
*/

router.get('/posts/:perPage/:currentPage/',postController.getPost);
router.get('/posts/:id',postController.getPostDetails);
module.exports=router;