const { StatusCodes } = require("http-status-codes");
const success = (message, data, code, status = true) => {
  const pattern = {
    success: {
      message: message,
      status: status,
      data: data,
      code: code ? code : StatusCodes.OK,

    }
  };
  return pattern
};
const failure = (e, type, code, param, status = false) => {
  const pattern = {
    error: {
      message: e.message,
      status: status,
      type: type,
      code: code ? code : StatusCodes.BAD_REQUEST,
      param: param,
    },
  };

  return pattern;
};
module.exports = {
  success, failure
}