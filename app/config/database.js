
const dotenv=require('dotenv');
var mongoose = require('mongoose');
let config = process.env;
var authentication =config.DB_USER && config.DB_PASS? config.DB_USER + ":" + config.DB_PASS + "@" : "";
var URI = "mongodb://" + authentication + config.DB_HOST + "/" + config.DB_NAME;
console.log('connectionString',URI);
mongoose.connect(URI, {
   useUnifiedTopology: false
}).then(con => {
    console.log('connected');
}).catch(err=>{
    console.log('error',err.message);
})
