const dotenv = require("dotenv");
const path = require("path");
process.env.NODE_ENV = (process.env.NODE_ENV)?process.env.NODE_ENV : 'dev';
//console.log('-----------'+process.env.NODE_ENV+'----------');

module.exports = function () {
  const config = dotenv.config({ path: path.resolve(__dirname, 'env/'+`${process.env.NODE_ENV}`+'.env')});
  if (config.error) {
    throw new Error(config.error);
  }
};
