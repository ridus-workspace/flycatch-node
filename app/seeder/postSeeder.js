
const fs=require('fs');
const mongoose=require('mongoose');
const postModel=require('../model/postsModel');
/*
*DB connection and env variable
*/
require('../config/dotenv')();
require('../config/database');

/*
*Read file from seeder
*/
const readPostFile=JSON.parse(fs.readFileSync(`${__dirname}/postsData.json`,'utf-8'));

/*
*Import posts data from collection
*/

const ImportData=async()=>{
    try{
     await postModel.create(readPostFile);
         process.exit();
     }catch(e){
        console.log(e.message);
    }
}
/*
*Delete all data from collection
*/

const deletData= async ()=>{
    try{
       await postModel.deleteMany();
        process.exit();
       
    }catch(e){
        console.log(e.message);
    }
}
/*
*call function based on argument
*/
if(process.argv[2]==='--import'){
    ImportData();
   
}else if(process.argv[2]==='--delete'){    
    deletData();
   
}
 