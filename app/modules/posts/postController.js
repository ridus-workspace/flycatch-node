const responseMessages = require('../../helpers/responseMessages');
const postService = require('./postService');
const { failure, success } = require("../../helpers/responseHelper");
const { StatusCodes } = require("http-status-codes");


/*
  @author : Riduvan
  method  : GET
  function: post list service
  request:''
  response:Array
  date : 19/10/2021
*/

const getPost = async (req, res) => {
    try {
        await postService.getPostsList(req, async (err, response) => {
            if (err)
                return res.status(StatusCodes.BAD_REQUEST).send(failure(err))
            else
                return res.status(StatusCodes.OK).send(success(responseMessages.messages.retrieveSuccess, response, StatusCodes.OK));
        })

    } catch (e) {
        return res.status(StatusCodes.BAD_REQUEST).send(failure(e));
    }
};

/*
  @author : Riduvan
  method  : GET
  function: get post Details from postservice
  request:'post id'
  response:Array
  date : 19/10/2021
*/
const getPostDetails = async (req, res) => {
    try {
        await postService.getPostDetails(req, async (err, response) => {
            if (err) {
                return res.status(StatusCodes.BAD_REQUEST).send(failure(err))
            }

            else
                return res.status(StatusCodes.OK).send(success(responseMessages.messages.retrieveSuccess, response, StatusCodes.OK));
        })

    } catch (e) {
        return res.status(StatusCodes.BAD_REQUEST).send(failure(e));
    }
};



module.exports = {
    getPost, getPostDetails
}