
const postsModel=require('../../model/postsModel');
const mongoose=require('mongoose');
const responseMessages=require('../../helpers/responseMessages');


/*
  @author : Riduvan
  method  : GET
  function: get post list
  request:'post id'
  response:Array
  date : 19/10/2021
*/

const getPostsList = async (req, cb) => {
    try {
      let perPage = req.params.perPage ? parseInt(req.params.perPage) : Constant.PER_PAGE;
      let currentPage = req.params.currentPage ? parseInt(req.params.currentPage) - 1 : Constant.CURRENT_PAGE;
        let condition={
            isDeleted:false
        }
        let result={}
         result.data = await postsModel.find(condition).select('title body').sort({'_id':'descending'}).limit(perPage)
         .skip(currentPage * perPage)
         .catch((error) => {
             return error;
         })
         result.count = await postsModel.find(condition).countDocuments();
        
        //pagination
        // if (req.query.limit) {
        //     const limit = req.query.limit * 1 || 1;
        //     const page = req.query.page * 1 || 1;
        //     const skip = (page - 1) * limit;
        //     data = data.skip(skip).limit(limit);
        // }

        // const resu = await data;
        cb('',result);
    }
    catch (error) {
        if (error.message != null) {
          errMsg = { message: error.message }
        } else {
          errMsg = { message: responseMessages.messages.Failed }
        }
        cb(errMsg);
      }
}

/*
  @author : Riduvan
  method  : GET
  function: get individual post Details
  request:'post id'
  response:Array
  date : 19/10/2021
*/
const getPostDetails = async (req, cb) => {
    try {
        let errMsg;
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
           
            errMsg = { message: responseMessages.messages.InvalidId };
            cb(errMsg)
        }
        let data = await postsModel.findById(req.params.id).select('title body');
        cb('',data);
    }
    catch (error) {
        if (error.message != null) {
          errMsg = { message: error.message }
        } else {
          errMsg = { message: responseMessages.messages.Failed }
        }
        cb(errMsg);
      }
}

module.exports={
    getPostsList,getPostDetails
}