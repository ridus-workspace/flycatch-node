const responseMessages = require('../../helpers/responseMessages');
const commentService = require('./commentService');
const { failure, success } = require("../../helpers/responseHelper");
const { StatusCodes } = require("http-status-codes");


/*
  @author : Riduvan
  method  : GET
  function: get all comments from service based on post id
  request:''
  response:Array
  date : 19/10/2021
*/
const getPostComments = async (req, res) => {
    try {
        await commentService.getPostComments(req, async (err, response) => {
            if (err) {
                return res.status(StatusCodes.BAD_REQUEST).send(failure(err))
            }

            else
                return res.status(StatusCodes.OK).send(success(responseMessages.messages.retrieveSuccess, response, StatusCodes.OK));
        })

    } catch (e) {
        return res.status(StatusCodes.BAD_REQUEST).send(failure(e));
    }
};
/*
  @author : Riduvan
  method  : GET
  function: save new comments
  request:''
  response:Array
  date : 19/10/2021
*/

const  savePostComments = async (req, res) => {
    try {
        await commentService.savePostComments(req, async (err, response) => {
            if (err) {
                return res.status(StatusCodes.BAD_REQUEST).send(failure(err))
            }

            else
                return res.status(StatusCodes.OK).send(success(responseMessages.messages.retrieveSuccess, '', StatusCodes.OK));
        })

    } catch (e) {
        return res.status(StatusCodes.BAD_REQUEST).send(failure(e));
    }
};



module.exports = {
    getPostComments, savePostComments
}