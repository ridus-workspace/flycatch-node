
const postCommentsModel=require('../../model/postCommentsModel');
const mongoose=require('mongoose');
const responseMessages=require('../../helpers/responseMessages');

/*
  @author : Riduvan
  method  : GET
  function: get comment list
  request:'post id'
  response:Array
  date : 19/10/2021
*/

const getPostComments = async (req, cb) => {
    try {
      if (!mongoose.Types.ObjectId.isValid(req.params.id)) {           
        errMsg = { message: responseMessages.messages.InvalidId };
        cb(errMsg)
    }
  
    let condition={
      postId:req.params.id,
      isDeleted:false
    }
    
        let data = await postCommentsModel.find(condition).select('name comment email createdAt updatedAt').sort({'_id':'descending'});
        cb('',data);
    }
    catch (error) {
        if (error.message != null) {
          errMsg = { message: error.message }
        } else {
          errMsg = { message: responseMessages.messages.Failed }
        }
        cb(errMsg);
      }
}

/*
  @author : Riduvan
  method  : GET
  function: save new comments againt post
  request:'post id'
  response:Array
  date : 19/10/2021
*/
const savePostComments = async (req, cb) => {
   try {
        let errMsg;
        const reqBody=req.body;
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {           
            errMsg = { message: responseMessages.messages.InvalidId };
            cb(errMsg)
        }
        reqBody.postId=req.params.id;
        await postCommentsModel.create(req.body);
         cb(null, responseMessages.messages.AddSuccess);
    }
    catch (error) {
        if (error.message != null) {
          errMsg = { message: error.message }
        } else {
          errMsg = { message: responseMessages.messages.Failed }
        }
        cb(errMsg);
      }
}

module.exports={
  getPostComments,savePostComments
}