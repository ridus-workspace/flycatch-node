const mongoose=require('mongoose');

const postSchema=new mongoose.Schema(
    {
        title: {
            type: String,
        },
        body: {
            type: String,
        },
        status: {
            type: Boolean,
            default: true
        },
        createdBy: {
            type: String,
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
    },
    {
        collection: "posts",
        versionKey: false,
        timestamps: true,
        toObject: {
            virtuals: true,
        },
        toJSON: {
            virtuals: true,
        },
    }
)
module.exports = mongoose.model('posts', postSchema);
postSchema.pre('save', function (next) {
    this.updatedAt = Date.now();
    return next();
  });
