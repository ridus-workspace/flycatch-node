const mongoose=require('mongoose');
const validator=require('validator');

const commentsSchema=new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'name must have']
        },
        email: {
            type: String,
            unique: true,
            lowercase: true,
            required: [true, 'email must have'],
            validate: [validator.isEmail, 'invalid email']
        },
        comment:{
            type: String, 
        },
        postId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "posts"
          },
        body: {
            type: String,
        },
        status: {
            type: Boolean,
            default: true
        },
        createdBy: {
            type: String,
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
    },
    {
        collection: "comments",
        versionKey: false,
        timestamps: true,
        toObject: {
            virtuals: true,
        },
        toJSON: {
            virtuals: true,
        },
    }
)
module.exports = mongoose.model('comments', commentsSchema);
commentsSchema.pre('save', function (next) {
    this.updatedAt = Date.now();
    return next();
  });
