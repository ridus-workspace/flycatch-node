const express = require('express');
const { json } = require('express');
const app = express();
const morgan = require('morgan');
// const router = express.Router();
app.use(express.json());
app.use(morgan('dev'));
require('./app/config/dotenv')();
require('./app/config/database');
require('./router')(app);
require("./app/config/cors")(app);
module.exports = app;
